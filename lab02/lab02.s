###########################################
# Lab 02 skeleton
# Group Member: Chao Zheng, Maria Glenski
#               Kenton Murray
###########################################

        .data		# Global Data
	A:      	.word   0, 1, 3, 4, 2, 2, 5, 5, 6, 7, 9, 8, 9, 12, 10	# array of numbers to be sorted
	space: 		.asciiz " "
	comma:		.asciiz ", "
        .text						# Program

###########################################
# Initialization code
###########################################
                
                .globl main
main:		la $s0 A                # load the starting address of the first
                                        # of element of the array of numbers into $s0
            	li $s1 15               # total number of elements to be added to list
                                        # (ok to "hard code" this)
		li $s2 0		# number of items currently added to list
                                 	# (initialized to 0)

            	add $s3 $s0 400		# location of first element of linked list
                                    	# (note that I assume it is at a "random" location
                                    	#  beyond the end of the array.)
		add $s4 $0 $0		# tail of linked list
                                    	# (set to 0 for empty list/NULL pointer)
		add $s5 $s3 $0		# assume $s5 holds the address of next list item;
					# at the start, we want to populate the first item,
                                    	# of the linked list so we simply copy the address in
                                    	# $s3 into $s5.

###########################################
# Loop code to building the linked list
#
# The structure of the node of link
#
#   | previous |
#   |----------|
#   |   data   |
#   |----------| 
#   |   next   |
#
###########################################

loadLoop:       lw $t0 0($s0)           # load the next element of the array
                add $a0 $t0 $0          # the value of the element will be passed as first argument
                add $a1 $s5 $0          # the address of current item will be passed as second argument	


		addi $s5 $s5 60		# when you insert a new item into a linked list, you
                                    	# might use a malloc command to allocate memory for the
                                    	# next struct; to mimic this, I simply add a random number
					# (60 for non contiguous allocation) to the first element
                                    	# of the linked list
                
                add $a2 $s5 $0          # the address of the new link will be passed as third argument 

                jal insert              # Call a function to add item to list
                addi $s0 $s0 4
                addi $s1 $s1 -1
                bne  $0 $s1 loadLoop  	# end the loadLoop
                 
                                    	# suggestion -- after building the list, add a sentinel
                                    	# to the head of the list; you may use the value -1 as 
                                    	# we will not test your code with any negative numbers
                #and $a0 $a0 $0
                #add $a0 $s3 $0
                #jal printll             # to test whether or not the list is built correctly, call
                                    	# the printll function here; the printll function will take
                                   	# the head of the linked list as an argument -- assuming you
                                    	# have not changed the original value of $s3, this is the data
                                    	# that you would want to send

                addi $s1 $s1 15         # reset the total number of items

###########################################
# Perform bubblesort on the list
###########################################

                j bubbleSort
                
#endbubble:      and $a0 $a0 $0
#                add $a0 $s3 $0
#                jal printll

endbubble:
###########################################
# Delete the duplicate elements
###########################################

                j delete

endDele:        and $a0 $a0 $0
                add $a0 $s3 $0
                jal printll

                li $v0 10               # syscall to exit cleanly from main only
                syscall                 # this ends execution 
                .end



###############################
# Bubble sort Pseudo-Code     #
# for(x=0; x<n; x++):         #
#   for(y=0; y<n-1; y++):     #
#     if(a[y]>a[y+1]):        #
#       swap(a[y],a[y+1])     #
###############################

###############################
# Nested Loop                 #
###############################

bubbleSort:     and $t3 $t3 $0          # set x=0
                addi $s6 $s1 -1         # set to n-1 = 14 (since n can be hardcode)
outerLoop:      and $t4 $t4 $0          # set y=0
                add $t5 $s3 $0          # go to the first link  
                slt $t9 $t3 $s1         # if x < n
                bne $0 $t9 innerLoop    # if yes, jump to the innerloop
                j endbubble             # if not, bubble sort is done

innerLoop:      lw $t6 4($t5)           # get the data stored in current link
                lw $t7 8($t5)           # get the address of the next link
                beq $s4 $t7 beforeJump
                lw $t8 4($t7)           # get the data stored in next link
                slt $t9 $t8 $t6         # if a[y] > a[y+1]
                bne $0 $t9 ifSwap        # if yes, jump to the ifswap
                addi $t4 $t4 1          # y ++ 
                slt $t9 $t4 $s6         # if y < n-1
                beq $0 $t9 beforeJump   # if not, jump to before jump
                add $t5 $t7 $0          # go to the next link
                j innerLoop             # loop again

beforeJump:     addi $t3 $t3 1          # x ++ 
                j outerLoop             # jump to outerloop

ifSwap:         add $a0 $t5 0           # the address of first item will be passed in swap as first argument
                add $a1 $t7 0           # the address of next item will be passed in swap as second argument
                jal swap                # call the swap function
                j innerLoop             # loop again 

###########################################
# INSERT FUNCTION 
###########################################
insert:         sw $a2 8($a1)           # link current link to the new link 
                sw $a1 0($a2)           # set previous link of new link 
                sw $a0 4($a2)           # set the data stored in new link
                sw $s4 8($a2)           # append the tail to the new link 
                jr $ra                  

###############################
# Swap Function               #
###############################
swap:                                   # elements to be swapped passed as $a0 (N), $a1 (N+1)
                lw $t0 8($a0)           # load N next pointer
                lw $t1 0($a0)           # load N back pointer
                lw $t2 8($t1)           # load N-1 next pointer
                sw $t0 8($t1)           # store N next pointer in N-1 next pointer reg
                lw $t0 0($a1)           # load N+1 back pointer
                sw $t1 0($a1)           # store N back pointer in N+1 back pointer reg
                lw $t1 8($a1)           # load N+1 next pointer
                beq $s4 $t1 endofList
                sw $t2 8($a1)           # store N-1 next pointer in N next pointer reg
                lw $t2 0($t1)           # load N+2 back pointer
                sw $t2 0($a0)           # store N+2 back pointer in N back pointer reg
                sw $t1 8($a0)           # store N+1 next pointer in N next pointer reg
                sw $t0 0($t1)           # store N+1 back pointer in N+2 back pointer reg
                jr $ra

endofList:      sw $t2 8($a1)
                sw $t1 8($a0)
                sw $s4  0($a0)
                jr $ra 
                 
###########################################
# DELETE FUNCTION -- you must write
###########################################

delete:         add $t0 $s3 $0          # reset the start position of the link 

delLoop:        lw $t1 4($t0)           # get the data stored in current element 
                lw $t2 8($t0)           # get the address of the next element
                beq $t2 $s4 endDele 
                lw $t3 4($t2)           # get the data stored in next element
                beq $t1 $t3 rmEle       # if curr_data = next_data, rm the duplicate
                add $t0 $t2 0
                j delLoop

rmEle:          lw $t4 8($t2)           # load N+2
                beq $t4 $s4 rmEndEle 
                sw $t4 8($t0)           # store addr of N+2 to N next pointer reg 
                sw $t0 0($t4)           # store addr of N to N+2 previous pointer reg 
                add $t0 $t4 0           # set curr element as N+2
                j delLoop

rmEndEle:       sw $s4 8($t0) 

###########################################
# Print linked list function -- printll
# Prints out a linked list given the head
# $a0 = head pointer
###########################################
			
printll:	addi $sp $sp -4		# Make room on the stack for the one reg we need
		sw $s0 0($sp)		# Save s0 to stack
			
		add $s0 $a0 $0		# Copy address to $s0 so a0 can be used for 
					# syscalls
		
pllloop:	lw $a0, 0($s0)		# Load previous address
		li $v0, 1		# Syscall to print an int
		syscall			# Print previous address

		li $v0, 4		# Syscall for string
		la $a0, space		# Spacer
		syscall			# Print out spacer
			
		lw $a0, 4($s0)		# Load value of item
		li $v0, 1		# Syscall to print an int
		syscall			# Print value
			
		li $v0, 4		# Syscall for string
		la $a0, space		# Spacer
		syscall			# Print out spacer
			
		lw $s0, 8($s0)		# Load next pointer
			
		add $a0 $s0 $0		# Print out next address
		li $v0, 1		# Syscall to print an int
		syscall			# Print next address

		beq $s0, $0, pend	# Avoid last comma and done
			
		li $v0, 4		# Syscall for string
		la $a0, comma		# Comma
		syscall			# Print out comma
		addi $a1, $a1, 4	# Next word
		b pllloop		# Next iteration of loop
			
pend:		lw $s0 0($sp)		# Load s0 from stack	
		addi $sp $sp 4		# Reset stack pointer
		jr $ra
