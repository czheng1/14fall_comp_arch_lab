CSE 30321 - Computer Architecture I - Lab 01
============================================

Problem A
---------

### QuestionA.1

|             | sa1_small_inp | xscale_small_inp  | speedup        | sa1_large_inp | xscale_large_inp | speedup |
|-------------|---------------| ------------------|----------------| --------------|------------------|---------|
| JPEG        | 0.015589      | 0.01446           | 7.7%           | 0.06854       | 0.06258          | 9.5%    |
| Mad         | 0.05859       | 0.04111           | 42.5%          | 2.85564       | 2.2437           | 27.3%   |
| Patricia    | 0.6924        | 0.1536            | 350.2%         | 4.1071        | 0.9457           | 334.3%  |

XScale slightly speedup JPEG program, speedup Mad program a lot and dramatically speed up Patricia program. 

### QuestionA.2

Following table shows the execution time of benchmarks with 1.33GHz clock rate StrongARM

|             | sa1_small_inp | xscale_small_inp  | speedup        | sa1_large_inp | xscale_large_inp | speedup |
|-------------|---------------| ------------------|----------------| --------------|------------------|---------|
| JPEG        | 0.011711      | 0.01446           | N/A            | 0.05153       | 0.06258          | N/A     |
| Mad         | 0.04405       | 0.04111           | 7.2%           | 2.1471        | 2.2437           | N/A     |
| Patricia    | 0.5206        | 0.1536            | 238.9%         | 3.0880        | 0.9457           | 226.5%  |

If the clock rate for strongARM is 1.33GHz, the XScale would slow down JPEG program, slightly speed up Mad Program 
with small input and slow down Mad Program with large input. While for Patricia program, the execution time has 
been dramatically shortened by applying XScale design

### QuestionA.3

Patricia benefit the most from the more sophisticated XScale configuration.

As shown in the Table1, several features of XScale are known,
1. XScale has 2X the amount of faster, on-chip memory
2. Time reuqired to access off-chip memory is 50% lower on XScale
3. The bandwidth between the microprocessor and off-chip memory is 2X higher with the XScale

Based on these features, we know that the executation time of program which requires frequent data access especially 
for accessing data on off-chip memory will be significantlly reduced

From the simulation result, we know that the program with highest data access rate for off-chip memory (largest dl1.access value combine with largest il1.access, dl1.miss_rate and il1.miss_rate values)will be sped up most

Thus, Patricia benchmark with highest percentage of dl1.access and il1.access to total instructions and highest il1.miss_rate will benefit the most from XScale design memory will be sped up most  

Problem B
--------- 

### QuestionB.1

| Iter_times       | 1000000        | 15000000       |
|------------------|----------------|----------------|
| sim_CPI          | 1.5018         | 1.2899         | 
| sim_IPC          | 0.6658         | 0.7753         |
| sim_num_insn     | 1004770757     | 3400495874     |
| sim_num_loads    | 214737512      | 320811225      |
| sim_num_stores   | 136812048.0000 | 194201372.0000 |
| sim_num_branches | 160241577      | 466723699      |
| dl1.accesses     | 353549585      | 530012618      |
| dl1.hits         | 353548964      | 530012276      |
| dl1.misses       | 621            | 342            |
| dl1.miss_rate    | 0.0000         | 0.0000         |
| il1.accesses     | 1434941201     | 4386121571     |
| il1.hits         | 1434940596     | 4386121142     | 
| il1.misses       | 605            | 429            |
| il1.miss_rate    | 0.0000         | 0.0000         |

**Table B1.1** Data metrics of initial code

Based on table B1.1 The execution time for the 1000000 iterations is: 

*sim_CPI * sim_num_insn * clock_rate = 1.5018 x 1004770757 x (1/(2000000000)) = 0.7545s*

The execution time for the 15000000 iterations is:

*sim_CPI * sim_num_insn * clock_rate = 1.2899 x 3400495874 x (1/(2000000000)) = 2.1931s*


| Iter_times       | 1000000        | 15000000       |
|------------------|----------------|----------------|
| sim_CPI          | 1.5216         | 1.4039         |
| sim_IPC          | 0.6572         | 0.7123         |
| sim_num_insn     | 1044372128     | 4027497045     |
| sim_num_loads    | 228237938      | 534561640      |
| sim_num_stores   | 136812053.0000 | 194201372.0000 |
| sim_num_branches | 172841860      | 666223973      |
| dl1.accesses     | 367050015      | 743763033      |
| dl1.hits         | 367049379      | 743762677      |
| dl1.misses       | 636            | 356            |
| dl1.miss_rate    | 0.0000         | 0.0000         |
| il1.accesses     | 1515043757     | 5654373817     |
| il1.hits         | 1515043140     | 5654373375     |
| il1.misses       | 617            | 442            |
| il1.miss_rate    | 0.0000         | 0.0000         |

**Table B1.2** Data metrics of code with default case checked last

Based on table B1.2 The execution time for the 1000000 iterations is: 

*sim_CPI * sim_num_insn * clock_rate = 1.5216 x 1044372128 x (1/(2000000000)) = 0.7946s *

The execution time for the 15000000 iterations is:

*sim_CPI * sim_num_insn * clock_rate = 1.4039 x 4027497045 x (1/(2000000000)) = 2.8271s*

By comparing the execution time, we know that the performance of the initial code is better. 
As shown in table B1.1 andtable B1.2, after moving the default case to the end the sim_num_insn increase, 
this is because the default case always happen and checking it last means all else-if statements
are always checked first.

### QuestionB.2

| Iter_times       | 1000000        | 15000000       |
|------------------|----------------|----------------|
| sim_CPI          | 1.5016         | 1.2905         |
| sim_IPC          | 0.6659         | 0.7749         |
| sim_num_insn     | 1002367167     | 3382434556     |
| sim_num_loads    | 214038451      | 315557422      |
| sim_num_stores   | 136812053.0000 | 194201367.0000 |
| sim_num_branches | 158435712      | 445669921      |
| dl1.accesses     | 352850527      | 524758813      |
| dl1.hits         | 352849873      | 524758437      |
| dl1.misses       | 654            | 376            |
| dl1.miss_rate    | 0.0000         | 0.0000         |
| il1.accesses     | 1431127197     | 4364949105     |
| il1.hits         | 1431126599     | 4364948677     |
| il1.misses       | 598            | 428            |
| il1.miss_rate    | 0.0000         | 0.0000         |


**Table B1.3** Data metrics of code with switch statement

Based on table B1.3 The execution time for the 1000000 iterations is: 

*sim_CPI * sim_num_insn * clock_rate = 1.5016 x 1002367167 x (1/(2000000000)) = 0.7526s*


The execution time for the 15000000 iterations is:

*sim_CPI * sim_num_insn * clock_rate = 1.2905 x 3382434556 x (1/(2000000000)) = 2.1825s*

After applying switch statement, the execution time reduced, for these many possible cases
the switch statement will be implemented by using hashmap like data structure internally, thus 
it is more efficient compared to normal if-else statement. Thus with limited number of possible 
cases, the if-else statement should be applied and the frequent used case should be placed on 
the top. If there are many possible cases, switch statement is recommended    

Problem C
--------- 

### QuestionC.1

The execution time for the no_unroll program is:
          
*sim_CPI * sim_num_insn * clock_rate = 1.4202 x 1538088235 x (1/(2000000000)) = 1.0922s*

### QuestionC.2

| Iter_times       | no_unroll          | init_unroll        | mac_unroll       |
|------------------|--------------------|--------------------|------------------|
| sim_CPI          | 1.4202             | 1.4307             | 1.4330           |
| sim_IPC          | 0.7041             | 0.6990             | 0.6979           |
| sim_num_insn     | 1538088235         | 1482088263         | 1471588068       |
| sim_num_loads    | 470006753          | 454006761          | 451006750        |
| sim_num_stores   | 230001157.0000     | 222001157.0000     | 220501152.0000   |
| sim_num_branches | 140004562          | 132004567          | 130504560        |
| dl1.accesses     | 564198009          | 540198017          | 535697998        |
| dl1.hits         | 561143395          | 537143086          | 532645343        |
| dl1.misses       | 3054614            | 3054931            | 3052655          |
| dl1.miss_rate    | 0.0054             | 0.0057             | 0.0057           |
| il1.accesses     | 2170515187         | 2106521511         | 2046971649       |
| il1.hits         | 2170514785         | 2106521097         | 2046971190       |
| il1.misses       | 402                | 414                | 459              |
| il1.miss_rate    | 0.0000             | 0.0000             | 0.0000           |

**Table B1.3** Data metrics of different version of loop rolling codes

The execution time for the init_unroll program is:

*sim_CPI * sim_num_insn * clock_rate = 1.4307 x 1482088263 x (1/(2000000000)) = 1.0602s*

The execution time for the mac_unroll program is:

*sim_CPI * sim_num_insn * clock_rate = 1.4330 x 1471588068 x (1/(2000000000)) = 1.0544s*

The mac_unroll version performs best. Based on table B1.3, the mac_unroll version has 
smallest value of instructions. Thus by unrolling "tight" loops the cost of branch penalty
is reduced and the number of instructions that control the loop is reducing. 

Problem D
---------

